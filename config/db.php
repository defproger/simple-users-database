<?php
ini_set('display_errors',1);
function db_getConnection()
{
    $user = 'root';
    $password = '';
    $db = 'users';
    $host = 'localhost';
    $port = 3306;

    static $dbh = null;
    if ($dbh != null) return $dbh;
    $dbh = new PDO("mysql:dbname=$db;host=$host;charset=utf8;port=$port", $user, $password,
        [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);
    return $dbh;
}


?>
