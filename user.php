<?php
session_start();

include "config/db.php";
include "config/function.php";


if ($_SESSION['enter'] !== "ENTEREND") {
    $_SESSION['name'] = 'dontenter';
    header("Location: login.php");
} else {
    $user = json_decode($_SESSION['user'], true) ;
}

if (isset($_POST['exit'])) {
    $_SESSION['name'] = 'dontenter';
    header("Location: login.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User page</title>
</head>
<body>

<?php
    echo $user['name'];
?>

<?php if ($user['galochka'] == 1):?>
    <a href="">Перейти к чему-то</a>
<?php elseif($user['galochka'] == 0): ?>
    <a href="">Вы не оплатили</a>
<?php endif;?>

<form action="user.php" method="post">
    <input type="hidden" name="exit" value="exit">
    <input type="submit" value="Выйти">
</form>

</body>
</html>
