<?php
session_start();

include "config/db.php";
include "config/function.php";

if (isset($_SESSION['enter'])) {
    if ($_SESSION['enter'] == 'dontenter') {
        echo '<script>alert("Вы ещё не вошли в аккаунт!");</script>';

    }
}

if (isset($_POST['num']) && isset($_POST['pass'])) {
    $user = db_getByNum('users',$_POST['num']);
    if ($user){
        if ($user['password'] == hashPassword($_POST['pass'])) {
            $_SESSION['enter'] = 'ENTEREND';
            $_SESSION['user'] = json_encode($user);
            header("Location: user.php");
        } else {
            echo '<script>alert("Пароль неверный!");</script>';
        }
    } else {
        echo '<script>alert("Такого пользователя не существует!");</script>';
    }
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>

<form action="login.php" method="post">
    <input type="number" name="num" placeholder="Номер телефона">
    <input type="password" name="pass" placeholder="Пароль">
    <input type="submit" value="Вход">
</form>

</body>
</html>
