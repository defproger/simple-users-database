<?php
include "config/db.php";
include "config/function.php";

if (isset($_POST['name']) && isset($_POST['num']) && isset($_POST['pass'])) {
    $users = db_getAll('users');
    $cheked = false;
    $massage = "Вы успешно зарегистрировались!";
    foreach ($users as $user) {
        if ($user['number'] == $_POST['num'] ) {
            $cheked = true;
            $massage = "Этот номер уже зарегистрирован!";
        }
    }
    if (!$cheked){
        db_insert('users', [
            'name' => $_POST['name'],
            'number' => $_POST['num'],
            'password' => hashPassword($_POST['pass'])
        ]);
    }
    echo '<script>alert("'.$massage.'");</script>';
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reg</title>
</head>
<body>


<form action="reg.php" method="post">
    <input type="text" name="name" placeholder="ФИО" required>
    <input type="number" name="num" placeholder="Номер телефона" required>
    <input type="password" name="pass" placeholder="Пароль" required>
    <input type="submit" value="Регистрация">
</form>

</body>
</html>
