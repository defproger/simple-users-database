<?php
session_start();

include "config/db.php";
include "config/function.php";

if (isset($_POST['pass'])) {
    if ($_POST['pass'] == 'rootforadmin') {
        $_SESSION['admin'] = 'admin';
    }
}
if (isset($_POST['id'])) {
    if ($_POST['galochka'] == null) {
        db_update('users', $_POST['id'], [
            'galochka' => 0
        ]);
    } elseif($_POST['galochka'] == "on") {
        db_update('users', $_POST['id'], [
            'galochka' => 1
        ]);
    }
    header("Location: admin.php");
}
if (isset($_POST['exit'])) {
    $_SESSION['admin'] = '';
    header("Location: admin.php");
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>АдминПанель</title>
</head>
<body>

<?php if ($_SESSION['admin'] !== "admin"): ?>
    <form action="admin.php" method="post">
        Вход в админпанель
        <input type="password" name="pass" placeholder="password">
        <input type="submit" value="Войти">
    </form>
<?php elseif ($_SESSION['admin'] == 'admin'): ?>
    <?php foreach (db_getAll('users') as $user): ?>
        <form action="admin.php" method="post">
            <?php print('<input type="hidden" name="id" value="'.$user['id'].'">'); ?>
            <?= $user['name'] ?> <?= $user['number'] ?>

            <?php if ($user['galochka'] == 1): ?>
                <input name="galochka" type="checkbox" checked>
            <?php elseif ($user['galochka'] == 0): ?>
                <input name="galochka" type="checkbox" >
            <?php endif; ?>

            <input type="submit" value="Изменить">
        </form>
    <?php endforeach; ?>
    <form action="admin.php" method="post">
        <input type="hidden" name="exit" value="exit">
        <input type="submit" value="Выйти">
    </form>
<?php endif; ?>

</body>
</html>
